# Referensi Dokumentasi

## Cara penggunaan

1. setelah git pull jika folder target tidak ada; maka jalankan perintah **mvn clean dependency:copy-dependencies package**
2. jika folder log tidak ada maka buat folder log sejajar dengan folder src; dengan file log bernama SpringBootMaster_API.log
3. Jika folder resources beserta isinya tidak ada maka buat folder resources di dalam src/main dengan isi folder application.properties dan log4j.properties

* isi file log4j.properties:

``` java
log4j.rootLogger=ALL, FILE1, stdout

# Direct log messages to a log file
#log4j.appender.FILE1=org.apache.log4j.RollingFileAppender
log4j.appender.FILE1=org.apache.log4j.DailyRollingFileAppender
#log4j.appender.file.MaxFileSize=2120KB
#log4j.appender.file.MaxBackupIndex=10
log4j.appender.FILE1.layout=org.apache.log4j.PatternLayout
log4j.appender.FILE1.DatePattern='.'yyyy-MM-dd-a
log4j.appender.FILE1.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss:SSS} %C:%L %-5p - %m%n
log4j.appender.FILE1.File=/home/aris/Documents/kode/teambchallenge4/log/SpringBootMaster_API.log

# Direct log messages to stdout
log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.Target=System.out
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
log4j.appender.stdout.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss:SSS} %C:%L %-5p - %m%n

```

* isi file application.properties

``` java
spring.datasource.url =jdbc:mysql://localhost:13306/bioskop
spring.datasource.username =root
spring.datasource.password =localhost
spring.jpa.hibernate.ddl-auto=none
spring.jpa.database-platform=org.hibernate.dialect.MySQL5InnoDBDialect
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5InnoDBDialect
spring.datasource.driverClassName=com.mysql.cj.jdbc.Driver
server.port=8083
```

* Ubah file tersebut berdasarkan seting komputer masing-masing
