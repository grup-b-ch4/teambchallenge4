package com.teamb.bioskop.infosys.teambchallange4.DTO;

import com.teamb.bioskop.infosys.teambchallange4.entities.SeatType;
import com.teamb.bioskop.infosys.teambchallange4.entities.Studio;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class SeatResponseDTO {

    private String seatNo;

    private List<Studio> studio;

    private SeatType seatAvail;
}
