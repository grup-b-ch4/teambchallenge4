package com.teamb.bioskop.infosys.teambchallange4.repositories;

import com.teamb.bioskop.infosys.teambchallange4.entities.Schedules;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchedulesRepository extends JpaRepository<Schedules, Integer> {
    @Query("select s from Schedules s where s.films.filmName like %:name%")
    public List<Schedules> getSchedulesByFilmName(@Param("name")String name);
}
