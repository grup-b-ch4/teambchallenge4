package com.teamb.bioskop.infosys.teambchallange4.services;

import com.teamb.bioskop.infosys.teambchallange4.entities.Studio;
import com.teamb.bioskop.infosys.teambchallange4.repositories.studioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class studioService {
    @Autowired
    private studioRepository repo;

    public List<Studio> getAll(){
        return repo.findAll();
    }
    public Studio createStudio(Studio studio){
        return repo.save(studio);
    }
    public Studio getStudio(String studio_id) {
        Optional<Studio> optionalStudio = this.repo.findById(studio_id);
        return optionalStudio.get();
    }
    public void deleteAllById(String studio_id){
        repo.deleteAllById(Collections.singleton(studio_id));
    }
    public Studio updateStudio( Studio studio) {
        this.getStudio(studio.getStudioId());
        return this.repo.save(studio);
    }


}
