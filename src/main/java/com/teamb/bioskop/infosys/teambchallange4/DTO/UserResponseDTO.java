package com.teamb.bioskop.infosys.teambchallange4.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
@AllArgsConstructor
public class UserResponseDTO {

    private Long Id;
    private String nama;
    private String username;
    private String password;
    private String email;

    @Override
    public String toString() {
        return "UserResponseDTO{" +
                "Id=" + Id +
                ", nama='" + nama + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
