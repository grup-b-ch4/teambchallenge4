package com.teamb.bioskop.infosys.teambchallange4.entities;

public enum AiringType {
    Y, N
}
