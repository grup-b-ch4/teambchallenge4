package com.teamb.bioskop.infosys.teambchallange4.controllers;

import com.teamb.bioskop.infosys.teambchallange4.DTO.RequestFilmByNameDTO;
import com.teamb.bioskop.infosys.teambchallange4.DTO.ScheduleRequestDTO;
import com.teamb.bioskop.infosys.teambchallange4.DTO.ScheduleResponseDTO;
import com.teamb.bioskop.infosys.teambchallange4.DTO.SchedulesPostResponseDTO;
import com.teamb.bioskop.infosys.teambchallange4.entities.Film;
import com.teamb.bioskop.infosys.teambchallange4.entities.Schedules;
import com.teamb.bioskop.infosys.teambchallange4.handler.ResponseHandler;
import com.teamb.bioskop.infosys.teambchallange4.services.SchedulesService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
public class SchedulesController {

    private static final Logger logger = LogManager.getLogger(SchedulesController.class);

    @Autowired
    private SchedulesService service;

    // GET ALL Schedules
    @GetMapping(value = "/schedules")
    public ResponseEntity<Object> getAll() {
        try {
            List<Schedules> result = service.getAll();
            for (Schedules schedules : result) {
                logger.info("GET ALL SCHEDULE DATA " + schedules.getFilms() + " || " +
                schedules.getStudioId() + " || " + schedules.getTanggalTayang() +
                " || " + schedules.getJamMulai() + " || " + schedules.getJamSelesai() +
                " || " + schedules.getHargaTiket());
            }
            return ResponseHandler.generateResponse("Data Dapat Tampil Semua", HttpStatus.OK, result);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }

    // GET ONE BY SCHEDULES_ID
    @GetMapping(value = "/schedules/{schedules_id}")
    public ResponseEntity<Object> getSchedules(@PathVariable Integer schedules_id) {
        try {
            Schedules result = service.getSchedules(schedules_id);
            logger.info("------------------------------------");
            logger.info("GET ONE SCHEDULE DATA " + result);
            logger.info("-------------------------------------");

            return ResponseHandler.generateResponse("Data " + schedules_id +" Dapat Tampil Semua", HttpStatus.OK, result);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }

    //POST SCHEDULE DATA
    @PostMapping(value = "/schedules")
    public ResponseEntity<Object> saveSchedules(@RequestBody ScheduleRequestDTO scheduleRequestDTO) {
        try {
            Schedules newSchedules = scheduleRequestDTO.convertToEntity();

            Schedules schedules = this.service.saveSchedules(newSchedules);
            SchedulesPostResponseDTO responseDTO = schedules.convertToResponsePost();
            logger.info("------------------------------------");
            logger.info("POST SCHEDULE DATA " + responseDTO);
            logger.info("-------------------------------------");

            return ResponseHandler.generateResponse("Data Post dapat ditambahkan", HttpStatus.OK, responseDTO);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }

    //UPDATE SCHEDULE DATA
    @PutMapping(value = "/schedules/{schedules_id}")
    public ResponseEntity<Object> updateSchedules(@PathVariable Integer schedules_id, @RequestBody ScheduleRequestDTO scheduleRequestDTO) {
        try {
            Schedules schedules = scheduleRequestDTO.convertToEntity();

            schedules.setScheduleId(schedules_id);

            Schedules updateSchedules = this.service.updateSchedules(schedules);
            logger.info("------------------------------------");
            logger.info("POST SCHEDULE DATA " + updateSchedules);
            logger.info("-------------------------------------");

            return ResponseHandler.generateResponse("Data telah terUpdate", HttpStatus.OK, updateSchedules);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }

    //DELETE SCHEDULES DATA
    @DeleteMapping(value = "/schedules/{schedules_id}")
    public ResponseEntity<Object> deleteSchedules(@PathVariable Integer schedules_id) {
        try {
            String schedules = service.deleteSchedules(schedules_id);
            logger.info("------------------------------------");
            logger.info(schedules);
            logger.info("-------------------------------------");
            return ResponseHandler.generateResponse("Data Telah terhapus", HttpStatus.OK, schedules);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }

    //GET SCHEDULES BY FILM NAME
    @PostMapping(value = "/schedules/filmname")
    public ResponseEntity<Object> getSchedulesByFilmName(@RequestBody RequestFilmByNameDTO requstFilmByNameDTO) {
        try {
            List<Schedules> result = service.getSchedulesByFilmName(requstFilmByNameDTO.getFilmName());
            for (Schedules schedules : result) {
                logger.info("GET SCHEDULE DATA BY FILM NAME: " + schedules.getFilms() + " || " +
                        schedules.getStudioId() + " || " + schedules.getTanggalTayang() +
                        " || " + schedules.getJamMulai() + " || " + schedules.getJamSelesai() +
                        " || " + schedules.getHargaTiket());
            }
            return ResponseHandler.generateResponse("Data schedules By Film Name Dapat Tampil Semua", HttpStatus.OK, result);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }
}
