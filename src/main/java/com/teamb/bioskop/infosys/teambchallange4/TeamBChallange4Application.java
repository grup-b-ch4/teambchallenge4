package com.teamb.bioskop.infosys.teambchallange4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeamBChallange4Application {

    public static void main(String[] args) {
        SpringApplication.run(TeamBChallange4Application.class, args);
    }

}
