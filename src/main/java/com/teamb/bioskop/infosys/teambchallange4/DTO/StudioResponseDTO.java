package com.teamb.bioskop.infosys.teambchallange4.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
@AllArgsConstructor
public class StudioResponseDTO {

    private String studioId;

    private String studioName;
}
