//
//package com.teamb.bioskop.infosys.teambchallange4.controllers;
//
//import com.teamb.bioskop.infosys.teambchallange4.entities.Available;
//import com.teamb.bioskop.infosys.teambchallange4.entities.Studio;
//import com.teamb.bioskop.infosys.teambchallange4.services.AvailableServices;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.Optional;
//
//@RestController
//public class AvailableController {
//
//    @Autowired
//    private AvailableServices services;
//
//    @GetMapping("/available")
//    public Iterable<Available> AvailableList() {
//        return services.getAll();
//    }
//    @PostMapping("/Available")
//    public void saveAvailable(@RequestBody Available available ) {
//        Available avail = new Available();
//        Studio studio = new Studio();
//
//        avail.setSeat_no(available.getSeat_no());
//        avail.setStudioId(available.getStudioId());
//
//        services.saveAvailable(avail);
//
//    }
//
//    @GetMapping("/available/{seat_no}")
//    public Optional<Available> getAvailable(@PathVariable String seat_no ) {
//        return services.getAvailable(seat_no);
//    }
//
//    @PutMapping("/Available/{seat_no}")
//    public void updateAvailable(@RequestBody Available available, @PathVariable String seat_no ){
//        services.updateAvailable(seat_no, available);
//    }
//
//    @DeleteMapping("/Available/{seat_no}")
//    public void deleteAvailable(@PathVariable String seat_no ){
//        services.deleteAvailable(seat_no);
//    }
//
//}
//
