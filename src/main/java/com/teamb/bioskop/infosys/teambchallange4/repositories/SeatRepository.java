package com.teamb.bioskop.infosys.teambchallange4.repositories;

import com.teamb.bioskop.infosys.teambchallange4.entities.Seat;
import com.teamb.bioskop.infosys.teambchallange4.entities.SeatType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.websocket.server.PathParam;
import java.util.List;

@Repository
public interface SeatRepository extends JpaRepository<Seat,String> {

    //CUSTOM QUERY: GET ALL SEAT BY AVAILABILITY
    @Query("SELECT s FROM Seat s WHERE s.seatAvail = :#{#seatAvail}")
    List<Seat> getSeatByAvailable(@PathParam("seatAvail") SeatType seatAvail);

}
