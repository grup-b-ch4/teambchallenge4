package com.teamb.bioskop.infosys.teambchallange4.DTO;

import com.teamb.bioskop.infosys.teambchallange4.entities.Studio;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
@AllArgsConstructor
public class StudioRequestDTO {

    private String studioId;

    private String studioName;

    public Studio convertToEntity(){
        return Studio.builder()
                .studioId(this.studioId)
                .studioName(this.studioName)
                .build();
    }
}
