package com.teamb.bioskop.infosys.teambchallange4.entities;

import com.teamb.bioskop.infosys.teambchallange4.DTO.SeatResponseDTO;
import lombok.*;
import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "seats")
public class Seat {

    @Id
    private String seatNo;

    @ManyToMany
    @JoinTable(
            name = "available",
            joinColumns = @JoinColumn(name = "seat_no"),
            inverseJoinColumns = @JoinColumn(name = "studio_id")
    )
    private List<Studio> studio;

    @Column(name = "seat_available")
    @Enumerated(EnumType.STRING)
    private SeatType seatAvail;

    public SeatResponseDTO convertToResponse() {
        return SeatResponseDTO.builder().seatNo(this.seatNo).studio(this.studio).seatAvail(this.seatAvail).build();
    }
}
