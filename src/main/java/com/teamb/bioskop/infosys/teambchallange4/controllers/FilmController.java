package com.teamb.bioskop.infosys.teambchallange4.controllers;
import com.teamb.bioskop.infosys.teambchallange4.errors.NotFound;
import com.teamb.bioskop.infosys.teambchallange4.DTO.FilmRequestDtO;
import com.teamb.bioskop.infosys.teambchallange4.DTO.FilmResponseDTO;
import com.teamb.bioskop.infosys.teambchallange4.entities.Film;
import com.teamb.bioskop.infosys.teambchallange4.response.ResponseHandler;
import com.teamb.bioskop.infosys.teambchallange4.services.FilmService;
import lombok.AllArgsConstructor;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;


import java.util.List;



@RestController
@AllArgsConstructor
public class FilmController {

    private static final Logger logger = LogManager.getLogger(FilmController.class);

    @Autowired
    private FilmService filmService;

    @GetMapping("/films")
    public Object getFilms() {
        try {
            logger.info("================================================================");
            logger.info("GET /films");
            List<Film> filmList = this.filmService.getFilms();
            for (Film film : filmList) {
                logger.info("getFilms() called  with " + film.getFilmCode() + " " + film.getFilmName() + " " + film.getAiringType());
            }
            return ResponseHandler.generateResponse("film list", HttpStatus.OK, filmList);
        }catch (Exception e){
            logger.error("Error in getFilms()", e);
            return ResponseHandler.generateErrorResponse("Error in get Films", HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

    @PostMapping("/film/add")
    public Object addFilm(@RequestBody FilmRequestDtO filmRequestDtO) {
        try {
            logger.info("================================================================");
            logger.info("POST /film/add");
            Film filmRequest = filmRequestDtO.convertToEntity();
            Film film = this.filmService.addFilm(filmRequest);

            FilmResponseDTO filmResponseDTO = film.convertToResponse();

            var res = ResponseEntity.status(HttpStatus.CREATED).body(filmResponseDTO);
            logger.info("create film " + film.toString());
            return ResponseHandler.generateErrorResponse("film created", HttpStatus.OK, filmResponseDTO);
        }catch (Exception e){
            logger.error("Error in addFilm()", e);
            return ResponseHandler.generateErrorResponse("Error in add Film", HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

    @PutMapping("/film/update/{id}")
    public Object updateFilm(@PathVariable Long id, @RequestBody FilmRequestDtO filmRequestDtO) {
        logger.info("================================================================");
        logger.info("PUT /film/update/" + id);
        try {
            Film film = filmRequestDtO.convertToEntity();
            film.setFilmCode(id);
            Film updateFilm = this.filmService.updateFilm(film);
            logger.info("update film "+ updateFilm.toString());
            return ResponseHandler.generateResponse("film updated", HttpStatus.OK, updateFilm);
        }catch (Throwable err){
            logger.error("error update film "+ err.getMessage());
            return ResponseHandler.generateResponse("error update film", HttpStatus.INTERNAL_SERVER_ERROR, null);
        }

    }

    @GetMapping("/film/{id}")
    public Object getFilmById(@PathVariable Long id) {
        logger.info("================================================================");
        logger.info("GET /film/" + id);
           try{
               Film film = this.filmService.getFilmById(id);
                FilmResponseDTO filmResponseDTO = film.convertToResponse();
                logger.info("get film by id "+ film.toString());
                return ResponseHandler.generateResponse("film found", HttpStatus.OK, filmResponseDTO);
            }catch (NotFound err) {
                err = new NotFound();
                return ResponseHandler.generateErrorResponse("film not found", HttpStatus.BAD_REQUEST, null);
        }

    }

    @DeleteMapping("/film/delete/{id}")
    public Object deleteFilm(@PathVariable Long id) {
        logger.info("================================================================");
        logger.info("DELETE /film/delete/" + id);
        try{
            filmService.deleteFilm(id);
            logger.info("delete film "+ id);
            return ResponseHandler.generateErrorResponse("film deleted", HttpStatus.OK, null);
        }catch (NotFound err){
            err = new NotFound();
            logger.error("error delete film "+ err.getMessage());
            return ResponseHandler.generateErrorResponse("error delete film", HttpStatus.INTERNAL_SERVER_ERROR, null);
        }

    }

    @PostMapping("/film/name")
    public Object getFilmsByName(@RequestBody Film film) {
        logger.info("================================================================");
        logger.info("POST /film/name");
        if(film.getFilmName().isEmpty()){
            NotFound err = new NotFound();
            logger.error("error get film by name "+ err.getMessage());
            return ResponseHandler.generateErrorResponse("error get film by name", HttpStatus.BAD_REQUEST, null);
        }else {
            Film filmList = this.filmService.getFilmByName(film.getFilmName());
            FilmResponseDTO filmResponseDTO = filmList.convertToResponse();
            logger.info("get film by name "+ filmList.toString());
            return ResponseHandler.generateResponse("film found", HttpStatus.OK, filmResponseDTO);
        }
    }
}




















