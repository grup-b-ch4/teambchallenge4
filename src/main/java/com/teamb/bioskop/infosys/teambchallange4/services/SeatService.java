package com.teamb.bioskop.infosys.teambchallange4.services;

import com.teamb.bioskop.infosys.teambchallange4.entities.Seat;
import com.teamb.bioskop.infosys.teambchallange4.entities.SeatType;
import com.teamb.bioskop.infosys.teambchallange4.repositories.SeatRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class SeatService {

    @Autowired
    private SeatRepository repo;

    //GET ALL
    public List<Seat> getAll(){
        return repo.findAll();
    }

    //GET ONE BY ID
    public Seat getOne(String id){
        Optional<Seat> optionalSeat = this.repo.findById(id);
        if (optionalSeat.isEmpty()){
            System.out.println("Error");
        }
        return optionalSeat.get();
    }

    //GET SEAT BY AVAILABLE
    public List<Seat> getSeatByAvailable(SeatType seatAvail){
        return repo.getSeatByAvailable(seatAvail);
    }

    //CREATE SEAT
    public Seat createSeat(Seat seat){
        return this.repo.save(seat);
    }

    //UPDATE SEAT
    public Seat updateSeat(Seat seat){
        this.getOne(seat.getSeatNo());
        return this.repo.save(seat);
    }

    //DELETE SEAT
    public void deleteSeat(String id){
        this.repo.deleteById(id);
    }

}
