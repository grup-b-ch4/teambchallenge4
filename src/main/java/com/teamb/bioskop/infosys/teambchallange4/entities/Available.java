//
//package com.teamb.bioskop.infosys.teambchallange4.entities;
//
//import lombok.Data;
//import lombok.Getter;
//import lombok.Setter;
//
//import javax.persistence.*;
//
//@Entity
//@Table(name = "available")
//@Getter
//@Setter
//public class Available {
//
//    @Id
//    @Column(length = 100, nullable = true, name ="seat_no")
//    private String seat_no;
//
//    @ManyToOne
//    @JoinColumn(name = "studio_id")
//    private Studio studioId;
//
//}
