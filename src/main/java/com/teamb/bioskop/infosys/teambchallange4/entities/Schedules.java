package com.teamb.bioskop.infosys.teambchallange4.entities;

import com.teamb.bioskop.infosys.teambchallange4.DTO.ScheduleResponseDTO;
import com.teamb.bioskop.infosys.teambchallange4.DTO.SchedulesPostResponseDTO;
import lombok.*;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

@Entity
@Table(name = "schedules")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Schedules {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer scheduleId;

    @ManyToOne
    @JoinColumn(name = "film_code")
    private Film films;

    @ManyToOne
    @JoinColumn(name = "studio_id")
    private Studio studioId;
    private Date tanggalTayang;

    private Time jamMulai;
    private Time jamSelesai;
    private String hargaTiket;

    public ScheduleResponseDTO convertToResponse(){
        return ScheduleResponseDTO.builder().
                films(this.films).studioId(this.studioId).tanggalTayang(this.tanggalTayang).
                jamMulai(this.jamMulai).
                jamSelesai(this.jamSelesai).hargaTiket(this.hargaTiket).build();
    }

    public SchedulesPostResponseDTO convertToResponsePost() {
        return SchedulesPostResponseDTO.builder().
                film_code(this.films.getFilmCode()).studioId(this.studioId.getStudioId()).
                tanggalTayang(this.tanggalTayang).jamMulai(this.jamMulai).
                jamSelesai(this.jamSelesai).hargaTiket(this.hargaTiket).build();
    }

    @Override
    public String toString() {
        return "Schedules{" +
                "scheduleId=" + scheduleId +
                ", films=" + films +
                ", studioId=" + studioId +
                ", tanggalTayang=" + tanggalTayang +
                ", jamMulai=" + jamMulai +
                ", jamSelesai=" + jamSelesai +
                ", hargaTiket='" + hargaTiket + '\'' +
                '}';
    }
}
