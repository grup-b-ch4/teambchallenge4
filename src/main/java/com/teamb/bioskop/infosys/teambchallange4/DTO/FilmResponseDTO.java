package com.teamb.bioskop.infosys.teambchallange4.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.teamb.bioskop.infosys.teambchallange4.entities.AiringType;
import com.teamb.bioskop.infosys.teambchallange4.entities.Film;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class FilmResponseDTO {

    private Long filmCode;
    private String filmName;
    private AiringType airingType;

}
