package com.teamb.bioskop.infosys.teambchallange4.entities;


import com.teamb.bioskop.infosys.teambchallange4.DTO.UserResponseDTO;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "user")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class User {
    @Id
    private Long Id;
    @Column(length = 100, nullable = false, name = "nama")
    private String nama;
    @Column(length = 100, nullable = false, name = "username")
    private String username;
    @Column(length = 100, nullable = false, name = "password")
    private String password;
    @Column(length = 100, nullable = false, name = "email")
    private String email;
    public UserResponseDTO convertToResponse(){
        return UserResponseDTO.builder()
                .Id(this.Id)
                .nama(this.nama)
                .username(this.username)
                .password(this.password)
                .email(this.email)
                .build();
    }

    @Override
    public String toString() {
        return "User{" +
                "Id=" + Id +
                ", nama='" + nama + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
