package com.teamb.bioskop.infosys.teambchallange4.controllers;

import com.teamb.bioskop.infosys.teambchallange4.DTO.StudioRequestDTO;
import com.teamb.bioskop.infosys.teambchallange4.DTO.StudioResponseDTO;
import com.teamb.bioskop.infosys.teambchallange4.entities.Studio;
import com.teamb.bioskop.infosys.teambchallange4.response.ResponseHandler;
import com.teamb.bioskop.infosys.teambchallange4.services.studioService;
import lombok.AllArgsConstructor;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
public class studioController {
    private static final Logger logger = LogManager.getLogger(studioController.class);
    @Autowired
    private studioService service;

    @GetMapping("/studio")
    public Object studioList() {
        try {
            List<Studio> result = service.getAll();
            for (Studio studiolist : result) {
                logger.info("GET ALL STUDIO DATA" + studiolist.getStudioId() + "||" + studiolist.getStudioName());
            }
            return ResponseHandler.generateResponse("Data Dapat Tampil Semua", HttpStatus.OK, result);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }


    @PostMapping("/studio")
    public Object CreateStudio(@RequestBody StudioRequestDTO studioRequestDTO) {
        try {
            Studio newStudio = studioRequestDTO.convertToEntity();

            Studio studio = this.service.createStudio(newStudio);
            StudioResponseDTO responseDTO = studio.convertToResponse();
            logger.info(" TAMBAH DATA" + responseDTO.getStudioId() + "||" + responseDTO.getStudioName());
            return ResponseHandler.generateResponse("Tambah Data", HttpStatus.OK, responseDTO);
        }
        catch (Throwable e)
        {
            logger.error("Data Tidak Dapat Ditambahkan");
            return ResponseHandler.generateErrorResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

    @GetMapping("/studio/{studio_id}")
    public Object getStudio(@PathVariable String studio_id) {
        try {
            Studio studio = this.service.getStudio(studio_id);

            StudioResponseDTO responseDTO = studio.convertToResponse();
            logger.info(" Data Get Studio By Id" + responseDTO.getStudioId() + "||" + responseDTO.getStudioName());
            return ResponseHandler.generateResponse("Ambil Daya Bedasarkan Id", HttpStatus.OK, responseDTO);
        } catch (Throwable e) {
            logger.error("Data Tidak Ada");
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.NOT_FOUND, null);
        }
    }

    @DeleteMapping("/studio/{studio_id}")
    public Object deleteAllById(@PathVariable String studio_id){
        try {
            this.service.deleteAllById(studio_id);
            logger.info("Data Berhasil di Hapus");
            return ResponseHandler.generateResponse("Data Terhapus", HttpStatus.OK, null);
        } catch (Throwable e) {
            logger.error("Data Gagal dihapus");
            return ResponseHandler.generateErrorResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }

    @PutMapping("/studio/{studio_id}")
    public Object updateStudio(@PathVariable String studio_id, @RequestBody StudioRequestDTO studioRequestDTO){
        try {
            Studio studio = studioRequestDTO.convertToEntity();

            studio.setStudioId(studio_id);

            Studio updateStudio = this.service.updateStudio(studio);
            StudioResponseDTO responseDTO = updateStudio.convertToResponse();
            logger.info("Data Berhasil diupdate");
            return ResponseHandler.generateResponse("Data Berhail diupdate", HttpStatus.OK,responseDTO );
        } catch (Throwable e) {
            logger.error("Data Gagal diupdate");
            return ResponseHandler.generateErrorResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, null);
        }

    }
}
