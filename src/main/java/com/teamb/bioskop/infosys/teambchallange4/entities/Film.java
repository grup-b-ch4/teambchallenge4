package com.teamb.bioskop.infosys.teambchallange4.entities;

import com.teamb.bioskop.infosys.teambchallange4.DTO.FilmResponseDTO;
import lombok.*;

import javax.persistence.*;

@Entity

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "films")
public class Film {

    @Id
    private Long filmCode;

    @Column(length = 100, nullable = false, name = "film_name")
    private String filmName;

    @Column(name = "airing")
    @Enumerated(EnumType.STRING)
    private AiringType airingType;

    public FilmResponseDTO convertToResponse() {
        return FilmResponseDTO.builder().filmCode(this.filmCode).filmName(this.filmName).airingType(this.airingType).build();
    }

    @Override
    public String toString() {
        return "Film{" +
                "filmCode=" + filmCode +
                ", filmName='" + filmName + '\'' +
                ", airingType=" + airingType +
                '}';
    }
}
