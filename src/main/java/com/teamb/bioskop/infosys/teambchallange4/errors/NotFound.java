package com.teamb.bioskop.infosys.teambchallange4.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus
public class NotFound extends RuntimeException{
    public NotFound() {
        super();
    }

}
