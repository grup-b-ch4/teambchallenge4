package com.teamb.bioskop.infosys.teambchallange4.repositories;

import com.teamb.bioskop.infosys.teambchallange4.entities.Film;



import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface FilmRepository extends JpaRepository<Film, Long> {
    @Query(value = "Select * from films where film_name = ?;", nativeQuery = true)
    Film getFilmByName(String filmName);
}
