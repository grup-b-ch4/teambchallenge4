package com.teamb.bioskop.infosys.teambchallange4.controllers;


import com.teamb.bioskop.infosys.teambchallange4.DTO.UserRequestDTO;
import com.teamb.bioskop.infosys.teambchallange4.DTO.UserResponseDTO;
import com.teamb.bioskop.infosys.teambchallange4.entities.User;
import com.teamb.bioskop.infosys.teambchallange4.handler.UserResponseHandler;
import com.teamb.bioskop.infosys.teambchallange4.services.userService;
import lombok.AllArgsConstructor;


import lombok.Data;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@Data
public class userController {

    private static final Logger logger = LogManager.getLogger(userController.class);
    @Autowired
    private final userService service;

    //Get All User Data
    @GetMapping(value = "/user")
    public ResponseEntity<Object> getAll() {
        try {
            List<User> result = service.getAll();
            for (User user : result) {
                logger.info("GET ALL USER DATA" + user.getId() + "||"
                        + user.getNama() + "|| " + user.getUsername() + " ||"
                        + user.getPassword() + " ||" + user.getEmail());
            }
            return UserResponseHandler.generateResponse("Data Dapat Tampil", HttpStatus.OK, result);
        } catch (Exception e) {
            return UserResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }

    //Post New Data User
    @PostMapping(value = "/user")
    public ResponseEntity<Object> createUser(@RequestBody UserRequestDTO userRequestDTO) {
        try {
            User newUser = userRequestDTO.convertToEntity();
            User user = this.service.insertUser(newUser);
            UserResponseDTO userResponseDTO = user.convertToResponse();
            logger.info("------------------------------------");
            logger.info("GET POST USER DATA " + userResponseDTO);
            logger.info("-------------------------------------");

            return UserResponseHandler.generateResponse("Data Tampil Semua", HttpStatus.OK, userResponseDTO);
        } catch (Exception e) {
            return UserResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }

    // Get User Data by ID
    @GetMapping(value = "/user/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable Long id) {
        try {
            User result = service.getUserById(id);
            logger.info("------------------------------------");
            logger.info("GET ONE USER DATA " + result);
            logger.info("-------------------------------------");
            return UserResponseHandler.generateResponse("Data dapat Tampil", HttpStatus.OK, result);
        } catch (Exception e) {
            return UserResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }

    //Update Data User
    @PutMapping(value = "/user/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable Long id, @RequestBody UserRequestDTO userRequestDTO) {
        try {
            User user = userRequestDTO.convertToEntity();
            user.setId(id);

            User updateUser = this.service.updateUser(user);
            logger.info("------------------------------------");
            logger.info("UPDATE USER DATA " + updateUser);
            logger.info("-------------------------------------");

            return UserResponseHandler.generateResponse("Data Terupdate", HttpStatus.OK, updateUser);
        } catch (Exception e) {
            return UserResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }

    //Delete User by ID
    @DeleteMapping(value = "/user/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable Long id) {
        try {
            String user = service.deleteUser(id);
            logger.info("------------------------------------");
            logger.info(user);
            logger.info("-------------------------------------");
            return UserResponseHandler.generateResponse("Data Telah Dihapus", HttpStatus.OK, user);
        } catch (Exception e) {
            return UserResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }
}


