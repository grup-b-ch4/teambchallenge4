package com.teamb.bioskop.infosys.teambchallange4.services;

import com.teamb.bioskop.infosys.teambchallange4.entities.Schedules;
import com.teamb.bioskop.infosys.teambchallange4.repositories.SchedulesRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class SchedulesService {

    private SchedulesRepository repo;

    public List<Schedules> getAll(){
        return repo.findAll();
    }

    public Schedules saveSchedules(Schedules schedules){
        return this.repo.save(schedules);
    }

    public Schedules getSchedules(Integer schedule_id) {
        Optional<Schedules> optionalSchedule = this.repo.findById(schedule_id);
        return optionalSchedule.get();
    }
    public Schedules updateSchedules(Schedules schedules) {
        this.getSchedules(schedules.getScheduleId());
        return this.repo.save(schedules);
    }

    public String deleteSchedules(Integer schedule_id) {
        this.repo.deleteById(schedule_id);
        return "Data sudah terhabus";
    }

    public List<Schedules> getSchedulesByFilmName(String filmName) {
        List<Schedules> schedulesByFilmName = this.repo.getSchedulesByFilmName(filmName);

        return schedulesByFilmName;
    }

}
