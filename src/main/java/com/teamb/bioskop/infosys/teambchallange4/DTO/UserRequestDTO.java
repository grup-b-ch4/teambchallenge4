package com.teamb.bioskop.infosys.teambchallange4.DTO;

import com.teamb.bioskop.infosys.teambchallange4.entities.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
@AllArgsConstructor
public class UserRequestDTO {
    private Long Id;
    private String nama;
    private String username;
    private String password;
    private String email;
    public User convertToEntity() {
        return User.builder()
                .Id(this.Id)
                .nama(this.nama)
                .username(this.username)
                .password(this.password)
                .email(this.email)
                .build();

    }
}