package com.teamb.bioskop.infosys.teambchallange4.DTO;

import com.teamb.bioskop.infosys.teambchallange4.entities.Seat;
import com.teamb.bioskop.infosys.teambchallange4.entities.SeatType;
import com.teamb.bioskop.infosys.teambchallange4.entities.Studio;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class SeatRequestDTO {

    private String seatNo;

    private List<Studio> studioId;

    private SeatType seatAvail;

    public Seat convertToEntity(){
        return Seat.builder().seatNo(this.seatNo).studio(this.studioId).seatAvail(this.seatAvail).build();
    }


}
