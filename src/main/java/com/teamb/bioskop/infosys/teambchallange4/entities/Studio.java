package com.teamb.bioskop.infosys.teambchallange4.entities;

import com.teamb.bioskop.infosys.teambchallange4.DTO.StudioResponseDTO;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "studio")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Studio {

    @Id
    private String studioId;

    @Column(length = 50, nullable = false, name = "studio_name")
    private String studioName;
    public StudioResponseDTO convertToResponse(){
        return StudioResponseDTO.builder()
                .studioId(this.studioId)
                .studioName(this.studioName)
                .build();
    }

    @Override
    public String toString() {
        return "Studio{" +
                "studioId='" + studioId + '\'' +
                ", studioName='" + studioName + '\'' +
                '}';
    }
}
