package com.teamb.bioskop.infosys.teambchallange4.controllers;

import com.teamb.bioskop.infosys.teambchallange4.DTO.SeatRequestDTO;
import com.teamb.bioskop.infosys.teambchallange4.DTO.SeatResponseDTO;
import com.teamb.bioskop.infosys.teambchallange4.entities.Seat;
import com.teamb.bioskop.infosys.teambchallange4.entities.SeatType;
import com.teamb.bioskop.infosys.teambchallange4.response.ResponseHandler;
import com.teamb.bioskop.infosys.teambchallange4.services.SeatService;
import lombok.AllArgsConstructor;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class SeatController {

    private static final Logger logger = LogManager.getLogger(SeatController.class);

    @Autowired
    private SeatService service;

    //GET ALL SEATS
    @GetMapping("/seat")
    public ResponseEntity<Object> seatList(){
        try {
            List<Seat> seatList = service.getAll();
            logger.info("================================================================");
            for (Seat seat : seatList){
                logger.info("seatList() called with " + seat.getSeatNo() + " " + seat.getStudio() + " "  + seat.getSeatAvail() + "\n");
            }
            List<Seat> result = service.getAll();
            return ResponseHandler.generateResponse("Successfully retrieved seats", HttpStatus.OK, result);

        } catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
    }

    //GET ONE SEAT BY ID
    @GetMapping("/seat/{id}")
    public ResponseEntity<Object> getSeatById(@PathVariable String id){

        try {
            logger.info("================================================================");
            Seat seat = service.getOne(id);

            SeatResponseDTO responseDTO = seat.convertToResponse();
            logger.info("getSeatById() called with " + responseDTO.getSeatNo() + " " + responseDTO.getStudio() + " " + responseDTO.getSeatAvail() + "\n");
            return ResponseHandler.generateResponse("Sucessfully retrieved seat", HttpStatus.OK, responseDTO);

        } catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }

    //GET BY AVAILABLE
    @GetMapping("/seat-available/{seatAvail}")
    public ResponseEntity<Object> seatAvailable(@PathVariable("seatAvail") SeatType seatAvail){

        try {
            logger.info("================================================================");
            List<Seat> seatAvailable = service.getSeatByAvailable(seatAvail);

            for(Seat seat : seatAvailable){
                logger.info("seatAvailable() called with " + seat.getSeatNo() + " " + seat.getStudio() + " "  + seat.getSeatAvail() + "\n");
            }

            return ResponseHandler.generateResponse("Successfully retrieved data", HttpStatus.OK, seatAvailable);

        } catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }

    //CREATE SEAT
    @PostMapping("/seat")
    public ResponseEntity<Object> addSeat(@RequestBody SeatRequestDTO seatRequestDTO){

        try {
            Seat convertedSeat = seatRequestDTO.convertToEntity();
            Seat newSeat = this.service.createSeat(convertedSeat);
            SeatResponseDTO responseDTO = newSeat.convertToResponse();
            logger.info("================================================================");
            logger.info("addSeat() called with " + responseDTO.getSeatNo() + " " + responseDTO.getStudio() + " "  + responseDTO.getSeatAvail() + "\n");
            return ResponseHandler.generateResponse("Successfully added seat", HttpStatus.CREATED, responseDTO);

        } catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }

    //UPDATE SEAT
    @PutMapping("/seat/update/{seat_no}")
    public ResponseEntity<Object> updateSeat(@PathVariable String seat_no, @RequestBody SeatRequestDTO seatRequestDTO){

        try {
            Seat targetSeat = seatRequestDTO.convertToEntity();
            targetSeat.setSeatNo(seat_no);
            Seat updateSeat = this.service.updateSeat(targetSeat);
            SeatResponseDTO responseDTO = updateSeat.convertToResponse();
            logger.info("================================================================");
            logger.info("updateSeat() called with " + responseDTO.getSeatNo() + " " + responseDTO.getStudio() + " "  + responseDTO.getSeatAvail() + "\n");
            return ResponseHandler.generateResponse("Successfully updated seat", HttpStatus.CREATED, responseDTO);

        } catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }

    //DELETE SEAT
    @DeleteMapping("/seat/delete/{id}")
    public ResponseEntity<Object> deletedSeat(@PathVariable("id") String id){

        try {
            Seat deletedSeat = this.service.getOne(id);
            logger.info("================================================================");
            logger.info("deletedSeat() called with " + deletedSeat.getSeatNo() + " " + deletedSeat.getStudio() + " "  + deletedSeat.getSeatAvail() + "\n");
            service.deleteSeat(id);

            return ResponseHandler.generateResponse("Successfully deleted seat", HttpStatus.OK, null);
        } catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }
}
