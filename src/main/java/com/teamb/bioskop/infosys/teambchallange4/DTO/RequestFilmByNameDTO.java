package com.teamb.bioskop.infosys.teambchallange4.DTO;

import com.teamb.bioskop.infosys.teambchallange4.entities.Schedules;
import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RequestFilmByNameDTO {
    private String filmName;
}
