package com.teamb.bioskop.infosys.teambchallange4.services;

import com.teamb.bioskop.infosys.teambchallange4.entities.Film;
import com.teamb.bioskop.infosys.teambchallange4.repositories.FilmRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
@Data
public class FilmService {

    @Autowired
    private FilmRepository filmRepository;

    public List<Film> getFilms() {
        return this.filmRepository.findAll();
    }

    public Film addFilm(Film film) {
        return this.filmRepository.save(film);
    }

    public Film getFilmById(Long id) {
        Optional<Film> optionalFilm = this.filmRepository.findById(id);
        return optionalFilm.get();
    }

    public void deleteFilm(Long id) {
        this.filmRepository.deleteById(id);
    }

    public Film updateFilm(Film film) {
        this.getFilmById(film.getFilmCode());
        return this.filmRepository.save(film);
    }

    public Film getFilmByName(String filmName) {
        Film filmList = this.filmRepository.getFilmByName(filmName);
        return filmList;
    }


}
