package com.teamb.bioskop.infosys.teambchallange4.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.teamb.bioskop.infosys.teambchallange4.entities.Film;
import com.teamb.bioskop.infosys.teambchallange4.entities.Schedules;
import com.teamb.bioskop.infosys.teambchallange4.entities.Studio;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.sql.Time;
import java.util.Date;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class ScheduleRequestDTO {
    private Film films;
    private Studio studioId;
    private Date tanggalTayang;
    private Time jamMulai;
    private Time jamSelesai;
    private  String hargaTiket;
    public Schedules convertToEntity(){
        return Schedules.builder().films(this.films).studioId(this.studioId).tanggalTayang(this.tanggalTayang).
                jamMulai(this.jamMulai).jamSelesai(this.jamSelesai).hargaTiket(this.hargaTiket).build();
    }
}
