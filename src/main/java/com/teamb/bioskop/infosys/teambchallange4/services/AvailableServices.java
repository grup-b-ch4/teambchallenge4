//package com.teamb.bioskop.infosys.teambchallange4.services;
//
//import com.teamb.bioskop.infosys.teambchallange4.entities.Available;
//import com.teamb.bioskop.infosys.teambchallange4.repositories.AvailableRepository;
//import lombok.AllArgsConstructor;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//import java.util.Optional;
//
//@Service
//@AllArgsConstructor
//public class AvailableServices {
//    @Autowired
//    private AvailableRepository repo;
//
//    public List<Available> getAll() {
//        return repo.findAll();
//    }
//
//    public Available saveAvailable(Available available){
//       return this.repo.save(available);
//    }
//
//    public Optional<Available> getAvailable(String seat_no) {
//        return repo.findById(seat_no);
//    }
//    public void updateAvailable(String seat_no, Available available) {
//        repo.save(available);
//    }
//
//    public void deleteAvailable(String seat_no) {
//        repo.deleteById(seat_no);
//    }
//}
//
//
