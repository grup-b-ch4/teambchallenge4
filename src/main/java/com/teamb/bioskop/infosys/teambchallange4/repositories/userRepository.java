package com.teamb.bioskop.infosys.teambchallange4.repositories;

import com.teamb.bioskop.infosys.teambchallange4.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface userRepository extends JpaRepository <User, Long> {
}
