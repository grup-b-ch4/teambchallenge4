package com.teamb.bioskop.infosys.teambchallange4.services;

import com.teamb.bioskop.infosys.teambchallange4.entities.User;
import com.teamb.bioskop.infosys.teambchallange4.repositories.userRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class userService {
    /**
     * Get all user from table
     */
    @Autowired
    private userRepository repo;
    public List<User> getAll(){

        return repo.findAll();
    }
    public User insertUser(User user) {

        return  this.repo.save(user);
    }
    public User getUserById(Long Id){
        Optional<User> optionalUser = this.repo.findById(Id);
        return optionalUser.get();
    }
    public User updateUser(User user) {
        this.getUserById(user.getId());
       return this.repo.save(user);
    }
    public String deleteUser(Long Id) {
        this.repo.deleteById(Id);
        return "Data Sudah Terhapus";
    }

}
