package com.teamb.bioskop.infosys.teambchallange4.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.teamb.bioskop.infosys.teambchallange4.entities.Film;
import com.teamb.bioskop.infosys.teambchallange4.entities.Studio;
import lombok.*;

import java.sql.Time;
import java.util.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SchedulesPostResponseDTO {
    private Long film_code;

    private String studioId;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date tanggalTayang;

    @JsonFormat(pattern = "HH:mm:ss")
    private Time jamMulai;

    @JsonFormat(pattern = "HH:mm:ss")
    private Time jamSelesai;

    private String hargaTiket;

    @Override
    public String toString() {
        return "SchedulesPostResponseDTO{" +
                "film_code=" + film_code +
                ", studioId='" + studioId + '\'' +
                ", tanggalTayang=" + tanggalTayang +
                ", jamMulai=" + jamMulai +
                ", jamSelesai=" + jamSelesai +
                ", hargaTiket='" + hargaTiket + '\'' +
                '}';
    }
}
