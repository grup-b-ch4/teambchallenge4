package com.teamb.bioskop.infosys.teambchallange4.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.teamb.bioskop.infosys.teambchallange4.entities.Film;
import com.teamb.bioskop.infosys.teambchallange4.entities.Studio;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.sql.Time;
import java.util.Date;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class ScheduleResponseDTO {
    private Film films;

    private Studio studioId;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date tanggalTayang;

    @JsonFormat(pattern = "HH:mm:ss")
    private Time jamMulai;

    @JsonFormat(pattern = "HH:mm:ss")
    private Time jamSelesai;

    private String hargaTiket;

    @Override
    public String toString() {
        return "ScheduleResponseDTO{" +
                "films=" + films +
                ", studioId=" + studioId +
                ", tanggalTayang=" + tanggalTayang +
                ", jamMulai=" + jamMulai +
                ", jamSelesai=" + jamSelesai +
                ", hargaTiket='" + hargaTiket + '\'' +
                '}';
    }
}
