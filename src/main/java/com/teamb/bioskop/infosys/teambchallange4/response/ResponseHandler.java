package com.teamb.bioskop.infosys.teambchallange4.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class ResponseHandler {
    public static ResponseEntity<Object> generateResponse(String message, HttpStatus status, Object respObj){
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("Message", message);
        map.put("Status", status.value());
        map.put("Data", respObj);

        return new ResponseEntity<Object>(map,status);
    }

    public static Object generateErrorResponse(String s, HttpStatus internalServerError, Object o) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("Message", s);
        map.put("Status", internalServerError.value());
        map.put("Data", o);
        return new ResponseEntity<Object>(map,internalServerError);
    }
}
